#!/bin/bash

set -e

# Require length of first parameter to have 10 characters
[ ${#1} == "10" ] || exit

CADILINE=$1
CADI_LOWER=$(echo ${CADILINE} | tr '[:upper:]' '[:lower:]')

echo "Creating ${CADILINE}"

git clone "https://:@gitlab.cern.ch:8443/cms-b2g-hepdata/${CADI_LOWER}" ../"${CADI_LOWER}"

cp -r .gitlab ../"${CADI_LOWER}"
cp -r .gitignore ../"${CADI_LOWER}"
cp -r .gitlab-ci.yml ../"${CADI_LOWER}"
cp -r write_yaml.py ../"${CADI_LOWER}"

sed "s/B2G-XX-XXX/${CADILINE}/; s/B2G HEPData template/${CADILINE}/" README.md > ../"${CADI_LOWER}"/README.md

for dir in ../"${CADI_LOWER}"
do
  (
  cd "$dir" || exit
  git add -A
  git commit -m "prepare ${CADILINE} entry"
  git push origin HEAD
  )
done

echo "Done."