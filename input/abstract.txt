A search for Kaluza--Klein excited vector boson resonances, $\mathrm{W}_{\mathrm{KK}}$, decaying in cascade to three $\mathrm{W}$ bosons via a scalar radion $\mathrm{R}$, $\mathrm{W}_{\mathrm{KK}} \to \mathrm{WR} \to \mathrm{WWW}$, with two or three massive jets is presented.
The search is performed with proton-proton collision data recorded at $\sqrt{s}$ = 13 TeV, collected by the CMS experiment at the LHC, during 2016--2018, corresponding to an integrated luminosity of 138 $\mathrm{fb}^{-1}$.
Two final states are simultaneously probed, one where the two $\mathrm{W}$ bosons produced by the $\mathrm{R}$ decay are reconstructed as separate, large-radius, massive jets, and one where they are merged in a single large-radius jet.
The data observed are in agreement with the standard model expectations.      
Limits are set on the product of the $\mathrm{W}_{\mathrm{KK}}$ resonance cross section and branching fraction to three W bosons in an extended warped extra-dimensional model.
