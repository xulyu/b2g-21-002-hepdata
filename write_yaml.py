import hepdata_lib
import numpy as np

from hepdata_lib import Submission
submission = Submission()

submission.read_abstract("input/abstract.txt")
#submission.add_link("Webpage with all figures and tables", "http://cms-results.web.cern.ch/cms-results/public-results/publications/B2G-21-002/")
#submission.add_link("arXiv", "https://arxiv.org/abs/1904.04193")
#submission.add_record_id(1728701, "inspire")


from hepdata_lib import Table
from hepdata_lib import Variable, Uncertainty,RootFileReader

for ips in range(1,7):
    #loop over 6 variables at PS
    filename={1:"PS2_had_MJJ_16,17,18",2:"PS2_had_Mj_max_16,17,18",3:"PS2_had__h4q_max_w_max__h4q_max_w_max_qcd_max__16,17,18",4:"PS3_had_MJJJ_16,17,18",5:"PS3_had_Mj_max_16,17,18",6:"PS3_had___W_max_16,17,18"}
    if ips == 1:table_postfit = Table("m_jj distribution at PS2")
    if ips == 2:table_postfit = Table("m_j distribution at PS2")
    if ips == 3:table_postfit = Table("deep-WH distribution at PS2")
    if ips == 4:table_postfit = Table("m_jjj distribution at PS3")
    if ips == 5:table_postfit = Table("m_j distribution at PS3")
    if ips == 6:table_postfit = Table("deep-WH distribution at PS3")

    if ips == 1:tablestr="Distribution of $m_{\mathrm{jj}}$ for preselected events with $\mathrm{N}_{j}$ = 2"
    if ips == 2:tablestr="Distribution of $m_{\mathrm{j}}$ for preselected events with $\mathrm{N}_{j}$ = 2"
    if ips == 3:tablestr="Distribution of the deep-WH value of the highest-mass jet with $m_{\mathrm{j}}$ > 100 GeV for preselected events with $\mathrm{N}_{j}$ = 2"
    if ips == 4:tablestr="Distribution of $m_{\mathrm{jjj}}$ for preselected events with $\mathrm{N}_{j}$ = 3"
    if ips == 5:tablestr="Distribution of $m_{\mathrm{j}}$ for preselected events with $\mathrm{N}_{j}$ = 3"
    if ips == 6:tablestr="Distribution of the deep-WH value of the highest-mass jet with 60 < $m_{\mathrm{j}}$ < 100 GeV for preselected events with $\mathrm{N}_{j}$ = 3"
    table_postfit.description = tablestr
    posi={1:" (upper left)",2:" (upper center)",3:" (upper right)",4:" (lower left)",5:" (lower center)",6:" (lower right)"}
    table_postfit.location = "Data from figure 2"+posi[ips]
    table_postfit.keywords["observables"] = ["N"]
    table_postfit.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
    table_postfit.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering"]
    textfile = np.loadtxt("input/%s.txt"%(filename[ips]), skiprows=1)
    textfilepy = open("input/%s.txt"%(filename[ips]))
    varall={1:"$m_{\mathrm{jj}}$",2:"$m_{\mathrm{j}^{\mathrm{max}}}$",3:"$deep-WH^{\mathrm{max}}$",4:"$m_{\mathrm{jjj}}$",5:"$m_{\mathrm{j}^{\mathrm{max}}}$",6:"$deep-W^{\mathrm{max}}$"}
    varstr=varall[ips]
    mass = Variable(varstr, is_independent=True, is_binned=False, units="GeV")
    mass.values = textfile[:,0]
    signal1 = Variable("Signal with $m_{\mathrm{W}_{\mathrm{KK}}}$ = 1.5 TeV, $m_{\mathrm{R}}$ = 0.3 TeV", is_independent=False, is_binned=False, units="")
    signal1.values = textfile[:,9]
    signal2 = Variable("Signal with $m_{\mathrm{W}_{\mathrm{KK}}}$ = 2 TeV, $m_{\mathrm{R}}$ = 0.2 TeV", is_independent=False, is_binned=False, units="")
    signal2.values = textfile[:,10]
    signal3 = Variable("Signal with $m_{\mathrm{W}_{\mathrm{KK}}}$ = 2 TeV, $m_{\mathrm{R}}$ = 1 TeV", is_independent=False, is_binned=False, units="")
    signal3.values = textfile[:,11]
    signal4 = Variable("Signal with $m_{\mathrm{W}_{\mathrm{KK}}}$ = 2.5 TeV, $m_{\mathrm{R}}$ = 1 TeV", is_independent=False, is_binned=False, units="")
    signal4.values = textfile[:,12]

    totalbkg = Variable("Total background", is_independent=False, is_binned=False, units="")
    totalbkg.values = textfile[:,3]
    unc_totalbkg = Uncertainty("statistical uncertainty",is_symmetric=True)
    #unc_totalbkg.values = textfile[:,4]
    unc_bkg_py=[]
    for line in textfilepy:
        if "Mass" in line:continue
        unc_bkg_py.append(float(line.split(" ")[4]))
    unc_totalbkg.values = unc_bkg_py
    totalbkg.add_uncertainty(unc_totalbkg)
    #totalbkg.uncertainties.append(unc_totalbkg)
    #prefit = Variable("Pre-fit", is_independent=False, is_binned=False, units="")
    #prefit.values = textfile[:,9]
    qcd = Variable("Multijet", is_independent=False, is_binned=False, units="")
    qcd.values = textfile[:,5]
    vjets = Variable("W+jets", is_independent=False, is_binned=False, units="")
    vjets.values = textfile[:,6]
    top = Variable("$t\\bar{t}$, single t", is_independent=False, is_binned=False, units="")
    top.values = textfile[:,7]
    vv = Variable("Other", is_independent=False, is_binned=False, units="")
    vv.values = textfile[:,8]
    table_postfit.add_variable(mass)
    table_postfit.add_variable(totalbkg)
    table_postfit.add_variable(qcd)
    table_postfit.add_variable(vjets)
    table_postfit.add_variable(top)
    table_postfit.add_variable(vv)
    table_postfit.add_variable(signal1)
    table_postfit.add_variable(signal2)
    table_postfit.add_variable(signal3)
    table_postfit.add_variable(signal4)
    table_postfit.add_image("input/%s.pdf"%(filename[ips]))
    submission.add_table(table_postfit)

for ips in range(1,5):
    #SFs and unc.
    filename={1:"MwsfuncLL",2:"MwsfuncLH",3:"MwhsfuncHL",4:"MwhsfuncHH"}
    table_postfit = Table("scale factors in %s region"%(filename[ips][-2:]))
    
    if ips == 1:tablestr="scale factors (SFs) for W, $t^{2}$, and q/g matched jets in the low-$m_{\mathrm{j}}$ and low-$p_{\mathrm{T}}$ (LL) bin, as functions of the deep-W discriminant value."
    if ips == 2:tablestr="scale factors (SFs) for W, $t^{2}$, and q/g matched jets in the low-$m_{\mathrm{j}}$ and high-$p_{\mathrm{T}}$ (LH) bin, as functions of the deep-W discriminant value."
    if ips == 3:tablestr="scale factors (SFs) for $t^{2}$, $t^{3,4}$, and q/g matched jets in the high-$m_{\mathrm{j}}$ and low-$p_{\mathrm{T}}$ (HL) bin, as functions of the deep-WH discriminant value."
    if ips == 4:tablestr="scale factors (SFs) for $t^{2}$, $t^{3,4}$, and q/g matched jets in the high-$m_{\mathrm{j}}$ and high-$p_{\mathrm{T}}$ (HH) bin, as functions of the deep-WH discriminant value."
    table_postfit.description = tablestr
    posi={1:" (upper left)",2:" (upper right)",3:" (lower left)",4:" (lower right)"}
    table_postfit.location = "Data from figure 4"+posi[ips]
    table_postfit.keywords["observables"] = ["F1"]
    table_postfit.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
    table_postfit.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering","scale factors"]
    textfile = np.loadtxt("input/%s.txt"%(filename[ips]), skiprows=1)
    textfilepy = open("input/%s.txt"%(filename[ips]))
    varall={1:"deep-W",2:"deep-W",3:"deep-WH",4:"deep-WH"}
    varstr=varall[ips]
    mass = Variable(varstr, is_independent=True, is_binned=False, units="")
    mass.values = textfile[:,0]
    if ips in [1,2]:sfname=["$\mathrm{SF}^{W}$","$\mathrm{SF}^{t^{2}}$","$\mathrm{SF}^{q/g}$"]
    if ips in [3,4]:sfname=["$\mathrm{SF}^{t^{2}}$","$\mathrm{SF}^{t^{3,4}}$","$\mathrm{SF}^{q/g}$"]
    sf1 = Variable(sfname[0], is_independent=False, is_binned=False, units="")
    sf1.values = textfile[:,1]
    sf1unc = Uncertainty("statistical and parton shower uncertainties",is_symmetric=True)
    #unc_totalbkg.values = textfile[:,4]
    sf1unc_py,sf2unc_py,sf3unc_py=[],[],[]
    for line in textfilepy:
        if "Score" in line:continue
        sf1unc_py.append(float(line.split(" ")[2]))
        sf2unc_py.append(float(line.split(" ")[4]))
        sf3unc_py.append(float(line.split(" ")[6]))
    sf1unc.values = sf1unc_py
    sf1.add_uncertainty(sf1unc)

    sf2 = Variable(sfname[1], is_independent=False, is_binned=False, units="")
    sf2.values = textfile[:,3]
    sf2unc = Uncertainty("statistical and parton shower uncertainties",is_symmetric=True)
    #unc_totalbkg.values = textfile[:,4]
    sf2unc.values = sf2unc_py
    sf2.add_uncertainty(sf2unc)

    sf3 = Variable(sfname[2], is_independent=False, is_binned=False, units="")
    sf3.values = textfile[:,5]
    sf3unc = Uncertainty("statistical and parton shower uncertainties",is_symmetric=True)
    #unc_totalbkg.values = textfile[:,4]
    sf3unc.values = sf3unc_py
    sf3.add_uncertainty(sf3unc)

    table_postfit.add_variable(mass)
    table_postfit.add_variable(sf1)
    table_postfit.add_variable(sf2)
    table_postfit.add_variable(sf3)
    table_postfit.add_image("input/%s.pdf"%(filename[ips]))
    submission.add_table(table_postfit)

for ips in range(1,5):
    #SF corrected from 1 lep.
    filename={1:"wlepPS__LL0_top_decomp16,17,18_paper",2:"wlepPS__LH0_top_decomp16,17,18_paper",3:"wh4qqcdh4qwlepPS__HL0_top_decomp16,17,18_paper",4:"wh4qqcdh4qwlepPS__HH0_top_decomp16,17,18_paper"}
    varall={1:"deep-W",2:"deep-W",3:"deep-WH",4:"deep-WH"}
    regstr={1:"LL",2:"LH",3:"HL",4:"HH"}
    var,reg=varall[ips],regstr[ips]
    table_postfit = Table("%s in %s of the single-lepton sideband"%(var,reg))
    
    tablestr="The %s discriminant of the jet with highest mass in the single-lepton sideband for %s samples."%(var,reg)
    table_postfit.description = tablestr
    posi={1:" (upper left)",2:" (upper right)",3:" (lower left)",4:" (lower right)"}
    table_postfit.location = "Data from figure 5"+posi[ips]
    table_postfit.keywords["observables"] = ["N"]
    table_postfit.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
    table_postfit.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering"]
    textfile = np.loadtxt("input/%s.txt"%(filename[ips]), skiprows=1)
    textfilepy = open("input/%s.txt"%(filename[ips]))
    
    mass = Variable(var, is_independent=True, is_binned=False, units="")
    mass.values = textfile[:,0]
    sf1 = Variable("Totalbkg uncorrected", is_independent=False, is_binned=False, units="")
    sf1.values = textfile[:,3]

    sf2 = Variable("Totalbkg SF-corrected", is_independent=False, is_binned=False, units="")
    sf2.values = textfile[:,4]
    
    sf3 = Variable("$t^{3,4}$", is_independent=False, is_binned=False, units="")
    sf3.values = textfile[:,5]
    sf4 = Variable("$t^{2}$", is_independent=False, is_binned=False, units="")
    sf4.values = textfile[:,6]
    sf5 = Variable("W", is_independent=False, is_binned=False, units="")
    sf5.values = textfile[:,7]
    sf6 = Variable("q/g", is_independent=False, is_binned=False, units="")
    sf6.values = textfile[:,8]
    sf7 = Variable("Rest", is_independent=False, is_binned=False, units="")
    sf7.values = textfile[:,9]

    table_postfit.add_variable(mass)
    table_postfit.add_variable(sf1)
    table_postfit.add_variable(sf2)
    table_postfit.add_variable(sf3)
    table_postfit.add_variable(sf4)
    table_postfit.add_variable(sf5)
    table_postfit.add_variable(sf6)
    table_postfit.add_variable(sf7)
    table_postfit.add_image("input/%s.pdf"%(filename[ips]))
    submission.add_table(table_postfit)


for ips in range(1,6):
    #deep-W (WH) in CRs.
    filename={1:"CR1q_had___W_max_16,17,18",2:"CR2q_had__h4q_max_w_max__h4q_max_w_max_qcd_max__16,17,18",3:"CR3q_had__h4q_max_w_max__h4q_max_w_max_qcd_max__16,17,18",4:"CR4q_had___W_max_16,17,18",5:"CR6q_had___W_max_16,17,18"}
    varall={1:"$deep-W^{max}$",2:"$deep-WH^{max}$",3:"$deep-WH^{max}$",4:"$deep-W^{max}$",5:"$deep-W^{max}$"}
    varstr=varall[ips]
    cri,cris=ips,ips
    if ips==5:cri=6;cris="6"
    if ips==4:cris="45"
    obs="deep-W"
    if ips in [2,3]:obs="deep-WH"
    table_postfit = Table(obs+" in CR%s"%(cri))
    
    tablestr="Comparison of the distribution of data and simulated backgrounds, as a function of the %s discriminant value for the highest-mass jet in CR%s, after the scale factors have been applied."%(obs,cris)
    
    table_postfit.description = tablestr
    posi={1:" (upper left)",2:" (upper center)",3:" (upper right)",4:" (lower left)",5:" (lower right)"}
    table_postfit.location = "Data from figure 6"+posi[ips]
    table_postfit.keywords["observables"] = ["N"]
    table_postfit.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
    table_postfit.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering"]
    textfile = np.loadtxt("input/%s.txt"%(filename[ips]), skiprows=1)
    textfilepy = open("input/%s.txt"%(filename[ips]))
                          
    mass = Variable(varstr, is_independent=True, is_binned=False, units="")
    mass.values = textfile[:,0]
                          
    totalbkg = Variable("Total background", is_independent=False, is_binned=False, units="")
    totalbkg.values = textfile[:,3]
    unc_totalbkg = Uncertainty("statistical uncertainty",is_symmetric=True)
    #unc_totalbkg.values = textfile[:,4]
    unc_bkg_py=[]
    for line in textfilepy:
        if "Score" in line:continue
        uncv=float(line.split(" ")[2])
        if uncv==0:uncv=0.0001
        unc_bkg_py.append(uncv)
    #print(ips,unc_bkg_py)
    unc_totalbkg.values = unc_bkg_py
    totalbkg.add_uncertainty(unc_totalbkg)


    #totalbkg.uncertainties.append(unc_totalbkg)
    #prefit = Variable("Pre-fit", is_independent=False, is_binned=False, units="")
    #prefit.values = textfile[:,9]
    vjets = Variable("Multijet", is_independent=False, is_binned=False, units="")
    vjets.values = textfile[:,5]
    top = Variable("W+jets", is_independent=False, is_binned=False, units="")
    top.values = textfile[:,6]
    vv = Variable("$t\\bar{t}$, single t", is_independent=False, is_binned=False, units="")
    vv.values = textfile[:,7]
    vv1 = Variable("Other", is_independent=False, is_binned=False, units="")
    vv1.values = textfile[:,8]
    data = Variable("Data", is_independent=False, is_binned=False, units="")
    data.values = textfile[:,1]
    table_postfit.add_variable(mass)
    table_postfit.add_variable(data)
    table_postfit.add_variable(totalbkg)
    table_postfit.add_variable(vjets)
    table_postfit.add_variable(top)
    table_postfit.add_variable(vv)
    table_postfit.add_variable(vv1)
    table_postfit.add_image("input/%s.pdf"%(filename[ips]))
    submission.add_table(table_postfit)

for ips in range(1,4):
    #Jet Decomposition.
    filename={1:"j_max_3",2:"etAK8_3_norm",3:"jetAK_3_norm"}
    varall={1:"$m_{\mathrm{j}^{\mathrm{max}}}$",2:"deep-W",3:"deep-WH"}
    varstr=varall[ips]
    cri,cris=ips,ips
    obs="deep-W"
    if ips in [1]:obs="m_j"
    if ips in [3]:obs="deep-WH"
    table_postfit = Table("signal decomposition of "+obs+" in SR1-3")
    
    if ips==1:tablestr="The %s distributions for different jet types for SR1–3 events of the signal with $m_{\mathrm{W}_{\mathrm{KK}}}$ = 2.5 TeV, $m_{\mathrm{R}}$ = 0.2 TeV$ without deep-W (WH) constraints."%(varstr)
    if ips!=1:tablestr="The %s distribution normalized to unity for the shown components of of the signal with $m_{\mathrm{W}_{\mathrm{KK}}}$ = 2.5 TeV, $m_{\mathrm{R}}$ = 0.2 TeV$, respectively. The $t^{3,4}$ jets from the preselected sample, normalized to unity."%(varstr)

    table_postfit.description = tablestr
    posi={1:" (left)",2:" (center)",3:" (right)"}
    table_postfit.location = "Data from figure 7"+posi[ips]
    table_postfit.keywords["observables"] = ["N"]
    table_postfit.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
    table_postfit.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering"]
    textfile = np.loadtxt("input/%s.txt"%(filename[ips]), skiprows=1)
    textfilepy = open("input/%s.txt"%(filename[ips]))
    
    if ips==1:mass = Variable(varstr, is_independent=True, is_binned=False, units="GeV")
    if ips!=1:mass = Variable(varstr, is_independent=True, is_binned=False, units="")
    mass.values = textfile[:,0]
    
    if ips ==1:
        totalbkg = Variable("Total signal", is_independent=False, is_binned=False, units="")
        totalbkg.values = textfile[:,1]
        Fi_totalbkg = Uncertainty("statistical uncertainty",is_symmetric=True)
        #unc_totalbkg.values = textfile[:,4]
        unc_bkg_py=[]
        for line in textfilepy:
            if "m_{j}" in line:continue
            uncv=float(line.split(" ")[2])
            if uncv==0:uncv=0.0001
            unc_bkg_py.append(uncv)
        unc_totalbkg.values = unc_bkg_py
        totalbkg.add_uncertainty(unc_totalbkg)

    #totalbkg.uncertainties.append(unc_totalbkg)
    #prefit = Variable("Pre-fit", is_independent=False, is_binned=False, units="")
    #prefit.values = textfile[:,9]
    ist=1
    if ips==1:ist=3
    vjets = Variable("$R^{4q}$", is_independent=False, is_binned=False, units="")
    vjets.values = textfile[:,ist]
    top = Variable("$R^{3q}$", is_independent=False, is_binned=False, units="")
    top.values = textfile[:,ist+1]
    vv = Variable("$R^{lqq}$", is_independent=False, is_binned=False, units="")
    vv.values = textfile[:,ist+2]
    vv1 = Variable("W", is_independent=False, is_binned=False, units="")
    vv1.values = textfile[:,ist+3]
    rest = Variable("Rest", is_independent=False, is_binned=False, units="")
    rest.values = textfile[:,ist+4]
    table_postfit.add_variable(mass)
    if ips==1:table_postfit.add_variable(totalbkg)
    table_postfit.add_variable(vjets)
    table_postfit.add_variable(top)
    table_postfit.add_variable(vv)
    table_postfit.add_variable(vv1)
    table_postfit.add_variable(rest)
    table_postfit.add_image("input/%s.pdf"%(filename[ips]))
    submission.add_table(table_postfit)


for ips in range(1,6):
    #Observable in CRs.
    filename={1:"CR1q_had_MJJ_16,17,18",2:"CR2q_had_MJJ_16,17,18",3:"CR3q_had_MJJ_16,17,18",4:"CR4q_had_MJJJ_16,17,18",5:"CR6q_had_MJJJ_16,17,18"}
    varall={1:"$m_{\mathrm{jj}}$",2:"$m_{\mathrm{jj}}$",3:"$m_{\mathrm{jj}}$",4:"$m_{\mathrm{jjj}}$",5:"$m_{\mathrm{jjj}}$"}
    varstr=varall[ips]
    cri,cris=ips,ips
    if ips==5:cri=6;cris="6"
    if ips==4:cris="45"
    obs="m_jj"
    if ips>=4:obs="m_jjj"
    table_postfit = Table(obs+" in CR%s"%(cri))
    
    tablestr="The %s distribution for CR%s for data and simulation."%(varall[ips],cris)
    
    table_postfit.description = tablestr
    posi={1:" (upper left)",2:" (upper center)",3:" (upper right)",4:" (lower left)",5:" (lower right)"}
    table_postfit.location = "Data from figure 8"+posi[ips]
    
    table_postfit.keywords["observables"] = ["N"]
    table_postfit.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
    table_postfit.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering"]
    textfile = np.loadtxt("input/%s.txt"%(filename[ips]), skiprows=1)
    textfilepy = open("input/%s.txt"%(filename[ips]))
                          
    mass = Variable(varstr, is_independent=True, is_binned=False, units="")
    mass.values = textfile[:,0]
                          
    totalbkg = Variable("Total background", is_independent=False, is_binned=False, units="")
    totalbkg.values = textfile[:,3]
    unc_totalbkg = Uncertainty("statistical uncertainty",is_symmetric=True)
    #unc_totalbkg.values = textfile[:,4]
    unc_bkg_py=[]
    for line in textfilepy:
        if "Mass" in line:continue
        unc_bkg_py.append(float(line.split(" ")[4]))
    unc_totalbkg.values = unc_bkg_py
    totalbkg.add_uncertainty(unc_totalbkg)


    #totalbkg.uncertainties.append(unc_totalbkg)
    #prefit = Variable("Pre-fit", is_independent=False, is_binned=False, units="")
    #prefit.values = textfile[:,9]
    vjets = Variable("Multijet", is_independent=False, is_binned=False, units="")
    vjets.values = textfile[:,5]
    top = Variable("W+jets", is_independent=False, is_binned=False, units="")
    top.values = textfile[:,6]
    vv = Variable("$t\\bar{t}$, single t", is_independent=False, is_binned=False, units="")
    vv.values = textfile[:,7]
    vv1 = Variable("Other", is_independent=False, is_binned=False, units="")
    vv1.values = textfile[:,8]
    data = Variable("Data", is_independent=False, is_binned=False, units="")
    data.values = textfile[:,1]
    table_postfit.add_variable(mass)
    table_postfit.add_variable(data)
    table_postfit.add_variable(totalbkg)
    table_postfit.add_variable(vjets)
    table_postfit.add_variable(top)
    table_postfit.add_variable(vv)
    table_postfit.add_variable(vv1)
    table_postfit.add_image("input/%s.pdf"%(filename[ips]))
    submission.add_table(table_postfit)


for isr in range(1,7):
    #loop over 6 SRs; postfit
    if isr<4:table_postfit = Table("Post-fit distributions of m_jj distribution in SR%s"%(isr))
    if isr>=4:table_postfit = Table("Post-fit distributions of m_jjj distribution in SR%s"%(isr))
    if isr<4:table_postfit.description = "Post-fit distributions of the reconstructed triboson system ($m_{\mathrm{jj}}$) in data and simulation for SR%s."%(isr)
    if isr>=4:table_postfit.description = "Post-fit distributions of the reconstructed triboson system ($m_{\mathrm{jjj}}$) in data and simulation for SR%s."%(isr)
    posi={1:" (upper left)",2:" (upper center)",3:" (upper right)",4:" (lower left)",5:" (lower center)",6:" (lower right)"}
    table_postfit.location = "Data from figure 9"+posi[isr]

    table_postfit.keywords["observables"] = ["N"]
    table_postfit.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
    table_postfit.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering"]
    textfile = np.loadtxt("input/sr%s_postfit_bonly.txt"%(isr), skiprows=1)
    textfilepy = open("input/sr%s_postfit_bonly.txt"%(isr))
    varstr="$m_{\mathrm{jj}}$"
    if isr >= 4:varstr="$m_{\mathrm{jjj}}$"
    mass = Variable(varstr, is_independent=True, is_binned=False, units="TeV")
    mass.values = textfile[:,0]
    signal1 = Variable("Signal with $m_{\mathrm{W}_{\mathrm{KK}}}$ = 2.5 TeV, $m_{\mathrm{R}}$ = 0.2 TeV", is_independent=False, is_binned=False, units="")
    signal1.values = textfile[:,8]
    signal2 = Variable("Signal with $m_{\mathrm{W}_{\mathrm{KK}}}$ = 2.5 TeV, $m_{\mathrm{R}}$ = 1.25 TeV", is_independent=False, is_binned=False, units="")
    signal2.values = textfile[:,9]

    totalbkg = Variable("Total background", is_independent=False, is_binned=False, units="")
    totalbkg.values = textfile[:,3]
    unc_totalbkg = Uncertainty("total uncertainty",is_symmetric=True)
    #unc_totalbkg.values = textfile[:,4]
    unc_bkg_py=[]
    for line in textfilepy:
        if "Mass" in line:continue
        unc_bkg_py.append(float(line.split(" ")[4]))
    unc_totalbkg.values = unc_bkg_py
    totalbkg.add_uncertainty(unc_totalbkg)
    #totalbkg.uncertainties.append(unc_totalbkg)
    #prefit = Variable("Pre-fit", is_independent=False, is_binned=False, units="")
    #prefit.values = textfile[:,9]
    vjets = Variable("Multijet", is_independent=False, is_binned=False, units="")
    vjets.values = textfile[:,5]
    top = Variable("$t\\bar{t}$, single t", is_independent=False, is_binned=False, units="")
    top.values = textfile[:,6]
    vv = Variable("Other", is_independent=False, is_binned=False, units="")
    vv.values = textfile[:,7]
    data = Variable("Data", is_independent=False, is_binned=False, units="")
    data.values = textfile[:,1]
    table_postfit.add_variable(mass)
    table_postfit.add_variable(data)
    table_postfit.add_variable(totalbkg)
    table_postfit.add_variable(vjets)
    table_postfit.add_variable(top)
    table_postfit.add_variable(vv)
    table_postfit.add_variable(signal1)
    table_postfit.add_variable(signal2)
    #table_postfit.add_variable(prefit)
    table_postfit.add_image("input/sr%s_post-fit_b_.pdf"%(isr))
    submission.add_table(table_postfit)




#2D limit
filename = "input/expr_uperlimit_W_kk.root"
table = Table("exp_limit")
tableexp = Table("exp_limit_line")
tableexpm = Table("exp_minus_1sd_limit")
tableexpp = Table("exp_plus_1sd_limit")
tableobs = Table("obs_limit_line")

reader = RootFileReader(filename)
histograms = {}
xvars = {}
yvars = {}
zvars = {}

histograms['n2dll'] = reader.read_hist_2d("exp_limit")
xvars['n2dll'] = Variable("W_KK mass", is_independent=True, is_binned=False, units = "TeV")
yvars['n2dll'] = Variable("Radion mass", is_independent=True, is_binned=False, units = "TeV")
zvars['n2dll'] = Variable('upper limit on signal cross sections $\\times$ branching fraction to 3 W bosons', is_independent=False, is_binned=False, units = "pb")
xvars['n2dll'].values = histograms['n2dll']['x']
yvars['n2dll'].values = histograms['n2dll']['y']
zvars['n2dll'].values = histograms['n2dll']["z"]
table.add_variable(xvars["n2dll"])
table.add_variable(yvars["n2dll"])
table.add_variable(zvars["n2dll"])

histograms["exp"] = reader.read_graph("exp_limit_line")
histograms["68CL_plus1"] = reader.read_graph("exp_m1_limit_line")
histograms["68CL_minus1"] = reader.read_graph("exp_p1_limit_line")
histograms["obs"] = reader.read_graph("obs_limit_line")

xvars['68CL_plus1'] = Variable("W_KK mass", is_independent=True, is_binned=False, units = "TeV")
yvars['68CL_plus1'] = Variable("Radion mass", is_independent=False, is_binned=False, units = "TeV")
xvars['68CL_plus1'].values = histograms["68CL_plus1"]['x']
yvars['68CL_plus1'].values = histograms["68CL_plus1"]['y']
tableexpp.add_variable(xvars['68CL_plus1'])
tableexpp.add_variable(yvars['68CL_plus1'])

xvars['68CL_minus1'] = Variable("W_KK mass", is_independent=True, is_binned=False, units = "TeV")
yvars['68CL_minus1'] = Variable("Radion mass", is_independent=False, is_binned=False, units = "TeV")
xvars['68CL_minus1'].values = histograms["68CL_minus1"]['x']
yvars['68CL_minus1'].values = histograms["68CL_minus1"]['y']
tableexpm.add_variable(xvars['68CL_minus1'])
tableexpm.add_variable(yvars['68CL_minus1'])

xvars['exp'] = Variable("W_KK mass", is_independent=True, is_binned=False, units = "TeV")
yvars['exp'] = Variable("Radion mass", is_independent=False, is_binned=False, units = "TeV")
xvars['exp'].values = histograms["exp"]['x']
yvars['exp'].values = histograms["exp"]['y']
tableexp.add_variable(xvars['exp'])
tableexp.add_variable(yvars['exp'])

xvars['obs'] = Variable("W_KK mass", is_independent=True, is_binned=False, units = "TeV")
yvars['obs'] = Variable("Radion mass", is_independent=False, is_binned=False, units = "TeV")
xvars['obs'].values = histograms["obs"]['x']
yvars['obs'].values = histograms["obs"]['y']
tableobs.add_variable(xvars['obs'])
tableobs.add_variable(yvars['obs'])


table.name = "Expected upper limits on the signal cross section"
table.add_image("input/expr_uperlimit_W_kk.pdf")
table.description = "Expected upper limits at $95\%$ CL on the signal cross section $\\times$ branching fraction as functions of the $m_{\mathrm{W}_{\mathrm{KK}}}$ and $m_{\mathrm{R}}$ resonance masses."
table.location = "Data from figure 10."
table.keywords["observables"] = ["SIG"]
table.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
table.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering", "Cross Section"]

tableexp.name = "Expected median lower limit contour on the masses"
tableexp.add_image("input/expr_uperlimit_W_kk.pdf")
tableexp.description = "Expected median lower limit contour on the $m_{\mathrm{W}_{\mathrm{KK}}}$ and $m_{\mathrm{R}}$ plane."
tableexp.location = "Data from figure 10."
tableexp.keywords["observables"] = ["SIG"]
tableexp.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
tableexp.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering", "Cross Section"]

tableexpp.name = "Expected + 1 s.d. lower limit contour on the masses"
tableexpp.add_image("input/expr_uperlimit_W_kk.pdf")
tableexpp.description = "Expected $+ 1$ s.d. lower limit contour on the $m_{\mathrm{W}_{\mathrm{KK}}}$ and $m_{\mathrm{R}}$ plane."
tableexpp.location = "Data from figure 10."
tableexpp.keywords["observables"] = ["SIG"]
tableexpp.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
tableexpp.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering", "Cross Section"]

tableexpm.name = "Expected - 1 s.d. lower limit contour on the masses"
tableexpm.add_image("input/expr_uperlimit_W_kk.pdf")
tableexpm.description = "Expected - 1 s.d. lower limit contour on the $m_{\mathrm{W}_{\mathrm{KK}}}$ and $m_{\mathrm{R}}$ plane."
tableexpm.location = "Data from figure 10."
tableexpm.keywords["observables"] = ["SIG"]
tableexpm.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
tableexpm.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering", "Cross Section"]

tableobs.name = "Observed lower limit contour on the masses"
tableobs.add_image("input/expr_uperlimit_W_kk.pdf")
tableobs.description = "Observed lower limit contour on the $m_{\mathrm{W}_{\mathrm{KK}}}$ and $m_{\mathrm{R}}$ plane."
tableobs.location = "Data from figure 10."
tableobs.keywords["observables"] = ["SIG"]
tableobs.keywords["reactions"] = ["P P --> W_KK --> W RADION --> W W W"]
tableobs.keywords["phrases"] = ["radion","Triboson Resonances", "Proton-Proton Scattering", "Cross Section"]

submission.add_table(table)
submission.add_table(tableexp)
submission.add_table(tableexpp)
submission.add_table(tableexpm)
submission.add_table(tableobs)


for table in submission.tables:
    table.keywords["cmenergies"] = [13000]

submission.create_files("output")
